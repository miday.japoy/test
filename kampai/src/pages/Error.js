import { Link } from 'react-router-dom'

export default function Erorr(){

	return(

		<>
			<div>
				<row>
					<h1>404 - Not Found</h1>
					<p>The page you are looking for cannot be found.</p>
					<Link as="Link" to="/">Home page.</Link>
				</row>
			</div>
		</>

		);


};