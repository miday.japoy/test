import CreateProduct from '../components/CreateProduct';
import {Container, Col, Row} from 'react-bootstrap'



export default function ProductController(){
	


	return(

		<Container className="mt-3">
				<Row className="justify-content-center">
					<Col className="col-12 col-md-6 col-lg-6 border py-3 px-4 shadow bg-light mt-5">
						<CreateProduct />
					</Col>					
				</Row>
		</Container>

		

	);
};