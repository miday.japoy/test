import { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login() {


    const { user, setUser} = useContext(UserContext)
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {

    e.preventDefault();


    fetch('http://localhost:4000/users/login',{
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })

    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

             Swal.fire({
                title: 'Login Successfull!',
                icon: 'success',
                text: 'Kampai! You can now shop your favorite drinks'
             })
    
          } else {

              Swal.fire({
                 title: 'Authentication Failed!',
                icon: 'error',
             text: 'Check your login details.'
            })

        }
    })


    setEmail('');
    setPassword('');
   
    }
   

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

    }, [email, password]);

    return (

    (user.id !== null) ?
    <Redirect to="/login" />
    
        :
        <Container className="mt-5">
            <Row className="justify-content-center mt-5">
                <Col className="col-12 col-md-3 col-lg-4 border py-3 px-4 shadow bg-light mt-5">
                 <Form onSubmit={(e) => authenticate(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)} 
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)} 
                            required
                        />
                    </Form.Group>

                    { isActive ? 
                        <Button variant="primary" type="submit" id="submitBtn" className="mt-2">
                            Submit
                        </Button>
                        : 
                        <Button variant="danger" type="submit" id="submitBtn"  className="mt-2" disabled>
                            Submit
                        </Button>
                    }
                </Form>
            </Col>
            </Row>
        </Container>
    )
}
