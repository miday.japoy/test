
import { Link } from 'react-router-dom';
import { Fragment } from 'react';


export default function Edit(){

	return(

		<Fragment>
			<div>
				<row>
					<h1>404 - Not Found</h1>
					<p>The page you are looking for cannot be found.</p>
					<Link as="Link" to="/">Home page.</Link>
				</row>
			</div>
		</Fragment>

		);

}