
import { useState, useEffect } from 'react'
import { Container }  from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import Product from './pages/Product';
import Edit from './pages/Edit';
import myOrders from './pages/Orders';
import ProductController from './pages/ProductController';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Err from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext'


function App() {


const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear();
}


  useEffect( () => {
    let token = localStorage.getItem('token');
    fetch('http://localhost:4000/users/details', {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])



  return (
  <UserProvider value={{user, setUser, unsetUser}} >
    <Router>
      <AppNavbar/>
      <Container>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/product" component={Product} />
          <Route exact path="/orders" components={myOrders} />
          <Route exact path="/edit" components={Edit} />
          <Route exact path="/product/controller" component={ProductController} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="*" component={Err} />
        </Switch>        
      </Container>
    </Router>
  </UserProvider>
    
  );
}

export default App;
