import { Fragment, useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return(


		< Navbar expand="lg" bg="dark" variant="dark" className="px-3">
			
			<Navbar.Brand s={Link} to="/login">Kampai!</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="mr-auto">
			<Nav.Link href="#pricing">Home</Nav.Link>
			
			{
				(user.id !== null) ?
				<Fragment>
				<Nav.Link as={Link} to="/product">Product</Nav.Link>
				<Nav.Link as={Link} to="/product/controller">AddProduct</Nav.Link>
				<Nav.Link as={Link} to="/orders">Orders</Nav.Link>
				<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
				</Fragment>

				:

				<Fragment>
					<Nav.Link as={Link} to="/product">Product</Nav.Link>
					<Nav.Link as={Link} to="/register">Register</Nav.Link>
					<Nav.Link as={Link} to="/login" className="mx-auto">Login</Nav.Link>
				</Fragment>	

			}



			</Nav>
			</Navbar.Collapse>
		</Navbar>

		);



	}