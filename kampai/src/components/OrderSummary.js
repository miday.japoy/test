import { Card } from 'react-bootstrap';





export default function OrderSummary(props){

	const { cartedOrders, purchaseOn, total } = props;




	return(

		<>
			<Card>
				<Card.Header as="h5">{purchaseOn}</Card.Header>
				<Card.Body>
				<Card.Title>{cartedOrders}</Card.Title>
				<Card.Text>
				 {total}
				</Card.Text>
				</Card.Body>
			</Card>

		</>

		);
}