import {Card, Button} from 'react-bootstrap';


export default function ItemCard(props){

	const { myCart, setMyCart } = props;



	function addItem(item){


		const exist = myCart.find((x) => x._id === item._id);

		if(exist){
			setMyCart(myCart.map((x)=>  x._id === item._id ? {
				...exist, qty: exist.qty + 1
			} : x 
				)
			);
		} else {
					setMyCart(prevMyCart =>[...prevMyCart, {...item, qty: 1}])

		}
	}

console.log(props);

	
	return(


		<div className="col-11 col-md-6 col-lg-3 mx-0 mb-4">
			<Card className="p-0 overflow-hidden h-60 shadow">
				<Card.Img  variant="top" src={`http://localhost:4000/${props.productProps.image}`} className='img-fluid' />
				<Card.Body className='text-center'>
					<Card.Title>{props.productProps.name}</Card.Title>
					<h5>
							Php {props.productProps.price}
					</h5>					
					<Card.Text>
							{props.productProps.description}
					</Card.Text>

					<Button variant="primary" onClick={() => addItem(props.productProps)}>Add to Cart</Button>
				</Card.Body>
			</Card>			
		</div>

	);
};